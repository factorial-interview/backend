# README

Run project in Docker with Docker Compose:
```sh
docker compose up
```

Run project (Ruby 3.0.3):
```sh
bundle install && rails s -b 0.0.0.0
```

Conventional commits:
```sh
npx standard-version
```

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
