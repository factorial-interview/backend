# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/factorial-interview/backend/compare/v1.0.0...v1.0.1) (2021-11-29)


### Bug Fixes

* **test:** Added rspec ([e405bfe](https://gitlab.com/factorial-interview/backend/commit/e405bfe3a2d1a6e03bff7a9acc239fd05d1ed929))

## 1.0.0 (2021-11-29)
